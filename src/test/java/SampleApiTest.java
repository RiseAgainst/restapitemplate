import api.Configuration;
import api.PropertiesLoader;
import api.routes.GetPeople;
import api.matchers.PeopleAssert;
import api.response.People;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SampleApiTest {

    private static final Configuration config = PropertiesLoader.getConfig();


    @BeforeAll
    public static void setUp() {
        RestAssured.baseURI = config.getEndpoint();
    }


    @Test
    public void getCharacterInfo() {
        People response = GetPeople.getPeopleById("1");
        PeopleAssert.assertThat(response)
                .hasName("Luke Skywalker")
                .hasHeight("172")
                .hasMass("77")
                .hasGender("male");
    }

}


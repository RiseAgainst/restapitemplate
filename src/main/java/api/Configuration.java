package api;

import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

@Resource.Classpath("config.properties")
public interface Configuration {
    @Property("rest.endpoint")
    String getEndpoint();

}

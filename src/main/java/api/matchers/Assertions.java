package api.matchers;
import api.response.People;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
public class Assertions {

  /**
   * Creates a new instance of <code>{@link .PeopleAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static PeopleAssert assertThat(People actual) {
    return new PeopleAssert(actual);
  }

  /**
   * Creates a new <code>{@link Assertions}</code>.
   */
  protected Assertions() {
    // empty
  }
}

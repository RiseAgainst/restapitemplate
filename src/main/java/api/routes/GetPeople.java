package api.routes;

import api.response.People;

import static io.restassured.RestAssured.given;

public class GetPeople {

    public static <T> T getPeopleById(String id, Class<T> tClass) {
        return
                given().log().all()
                        .when()
                        .get("/people/" + id)
                        .then().log().all()
                        .extract()
                        .response().as(tClass);
    }

    // Метод вызывает get роут и возвращает обьект класса для работы с ответом
    public static People getPeopleById(String id) {
        return getPeopleById(id, People.class);
    }
}
